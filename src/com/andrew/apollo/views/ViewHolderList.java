/**
 * 
 */

package com.andrew.apollo.views;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrew.apollo.R;

/**
 * @author Andrew Neal
 */
public class ViewHolderList {

    public final ImageView mPeakOne, mPeakTwo;

    public final TextView mViewHolderLineOne;

    public int position;

    public ViewHolderList(View view) {
        mViewHolderLineOne = (TextView)view.findViewById(R.id.listview_item_line_one);
        mPeakOne = (ImageView)view.findViewById(R.id.peak_one);
        mPeakTwo = (ImageView)view.findViewById(R.id.peak_two);
    }
}
